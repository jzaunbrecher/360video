﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class UserGender
{
    private static string Gender;

    public static void SetGender(string gender)
    {
        Gender = gender;
    }
    public static string GetGender()
    {
        if(Gender != "Mand" && Gender != "Kvinde")
        {
            SetGender("Mand");
        }

        return Gender;
    }
}
