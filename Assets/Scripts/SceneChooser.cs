﻿using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.Video;

public class SceneChooser : MonoBehaviour
{
    private Camera camera;
    public GameObject Canvas;
    public Image Image;

    private int CurrentIndex;
    private float Speed = 200f;
    public float WaitInSeconds;
    private float FillAmount;
    private float CurrentValue;

    private string Scene;

    private void Start()
    {
        camera = GetComponent<Camera>();
        FillAmount = WaitInSeconds * Speed;

        camera.transform.parent.parent.rotation = Quaternion.Euler(0, 0, 0);
    }
    void Update()
    {
        Ray ray = new Ray(camera.transform.position, transform.forward);
        Debug.DrawRay(camera.transform.position, transform.forward * 100);

        if(Physics.Raycast(ray, out RaycastHit hit, 100))
        {
            if(hit.collider.tag == "LeftChoice" && CurrentIndex != 1)
            {
                Canvas.SetActive(true);
                CurrentValue = 0;
                CurrentIndex = -1;
            }
            else if(hit.collider.tag == "RightChoice" && CurrentIndex != 2)
            {
                Canvas.SetActive(true);
                CurrentValue = 0;
                CurrentIndex = -21;
            }
            else if(hit.collider.tag == "Reset")
            {
                Canvas.SetActive(false);
                CurrentValue = 0;
                CurrentIndex = 0;
            }
        }


        if(Canvas.activeSelf)
        {
            CurrentValue += Speed * Time.deltaTime;
            Image.fillAmount = CurrentValue / FillAmount;
        }

        if(CurrentValue >= FillAmount)
        {
            if(CurrentIndex == -1)
            {
                Debug.Log("Kage");
                //SceneManager.LoadSceneAsync("Kage");
                Scene = "Scene 1";
                NextScene();
            }
            else
            {
                Debug.Log("Kvinde");
                //SceneManager.LoadSceneAsync("OndtIMaven");
                Scene = "Scene 1";
                NextScene();
            }
        }
    }
    public void NextScene()
    {
        CurrentValue = 0;
        CurrentIndex = 0;
        Canvas.SetActive(false);

        SceneManager.LoadSceneAsync(Scene);
    }
}