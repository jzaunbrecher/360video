﻿using System;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;

public class VideoChanger : MonoBehaviour
{
    // These must alvays have the same size
    public VideoClip[] MalevideoClips;
    public VideoClip[] FemalevideoClips;

    private VideoClip[] videoClips;

    public string[] TextLeftChoice;
    public int[] IndexLeftChoice;
    public string[] TextCenterChoice;
    public int[] IndexCenterChoice;
    public string[] TextRightChoice;
    public int[] IndexRightChoice;

    public GameObject IndicatorCanvas;
    public GameObject LeftChoiceTekst;
    public GameObject CenterChoiceTekst;
    public GameObject RightChoiceTekst;

    public GameObject Canvas;
    public GameObject VideoOpject;

    public GameObject Exit;

    private VideoPlayer videoPlayer;
    private int videoClipIndex;
    private int CurrentIndex;
    private Camera camera;
    private Transform indicaterTarget;

    public Image Image;
    private float Speed = 200f;
    public float WaitInSeconds;
    private float FillAmount;
    private float CurrentValue;

    private List<Vector3> Original;
    private bool VideoIsDone;

    void Awake()
    {
        videoPlayer = VideoOpject.GetComponent<VideoPlayer>();
        camera = GetComponent<Camera>();
        FillAmount = WaitInSeconds * Speed;
    }

    private void Start()
    {
        if(UserGender.GetGender() == "Mand")
        {
            Debug.Log("Mand");
            videoClips = MalevideoClips;
        }
        else
        {
            Debug.Log("Kvinde");
            videoClips = FemalevideoClips;
        }

        if(videoClips.Length != TextLeftChoice.Length && videoClips.Length != IndexLeftChoice.Length && videoClips.Length != TextCenterChoice.Length && videoClips.Length != IndexCenterChoice.Length && videoClips.Length != TextRightChoice.Length && videoClips.Length != IndexRightChoice.Length)
        {
            Debug.Log("videoClips, TextLeftChoice, IndexLeftChoice, TextCenterChoice, IndexCenterChoice, TextRightChoice, IndexRightChoice must have the same size the don't");
        }
        else
        {
            Original = new List<Vector3>();
            Original.Add(LeftChoiceTekst.transform.parent.parent.position);
            Original.Add(CenterChoiceTekst.transform.parent.parent.position);
            Original.Add(RightChoiceTekst.transform.parent.parent.position);

            VideoIsDone = false;
            InvokeRepeating("CheckIfDone", .1f, .1f);

            PlayVideo();
        }
    }

    // Update is called once per frame
    void Update()
    {
        if(VideoIsDone)
        {
            //Debug.Log("Video Is Done");
            //Debug.Log(videoClipIndex);
            if(videoClipIndex >= -1)
            {
                RayCast();
            }
            else
            {
                End();
            }
        }
    }

    public void CheckIfDone()
    {
        long playerCurrentFrame = videoPlayer.GetComponent<VideoPlayer>().frame;
        long playerFrameCount = Convert.ToInt32(videoPlayer.GetComponent<VideoPlayer>().frameCount);

        //Debug.Log("playerCurrentFrame");
        //Debug.Log(playerCurrentFrame);

        //Debug.Log("playerFrameCount");
        //Debug.Log(playerFrameCount);

        //Debug.Log("playerCurrentFrame < playerFrameCount");
        //Debug.Log(playerCurrentFrame < playerFrameCount - 1);

        if(playerCurrentFrame < playerFrameCount - 1)
        {
            IndicatorCanvas.SetActive(false);
            LeftChoiceTekst.transform.parent.parent.gameObject.SetActive(false);
            CenterChoiceTekst.transform.parent.parent.gameObject.SetActive(false);
            RightChoiceTekst.transform.parent.parent.gameObject.SetActive(false);
        }
        else
        {
            VideoIsDone = true;

            CancelInvoke("CheckIfDone");

            //camera.transform.parent.parent.rotation = Quaternion.Euler(0, 0, 0);
            UnityEngine.XR.InputTracking.Recenter();

            if(IndexLeftChoice[videoClipIndex] == -1 && IndexCenterChoice[videoClipIndex] != -1 && IndexRightChoice[videoClipIndex] == -1)
            {
                IndicatorCanvas.SetActive(true);
                // Tekst Left Choice
                LeftChoiceTekst.transform.parent.parent.gameObject.SetActive(false);
                // Tekst Center Choice
                CenterChoiceTekst.GetComponent<TextMeshProUGUI>().text = TextCenterChoice[videoClipIndex];
                CenterChoiceTekst.transform.parent.parent.position = new Vector3(Original[1].x, -3, 10);
                CenterChoiceTekst.transform.parent.parent.gameObject.SetActive(true);
                // Tekst Right Choice
                RightChoiceTekst.transform.parent.parent.gameObject.SetActive(false);
            }
            else if(IndexLeftChoice[videoClipIndex] != -1 && IndexCenterChoice[videoClipIndex] == -1 && IndexRightChoice[videoClipIndex] != -1)
            {
                IndicatorCanvas.SetActive(true);
                // Tekst Left Choice
                LeftChoiceTekst.GetComponent<TextMeshProUGUI>().text = TextLeftChoice[videoClipIndex];
                LeftChoiceTekst.transform.parent.parent.position = new Vector3(Original[0].x + 3, -3, 10);
                LeftChoiceTekst.transform.parent.parent.gameObject.SetActive(true);
                // Tekst Center Choice
                CenterChoiceTekst.transform.parent.parent.gameObject.SetActive(false);
                // Tekst Right Choice
                RightChoiceTekst.GetComponent<TextMeshProUGUI>().text = TextRightChoice[videoClipIndex];
                RightChoiceTekst.transform.parent.parent.position = new Vector3(Original[2].x - 3, -3, 10);
                RightChoiceTekst.transform.parent.parent.gameObject.SetActive(true);
            }
            else if(IndexLeftChoice[videoClipIndex] <= -2 && IndexCenterChoice[videoClipIndex] <= -2 && IndexRightChoice[videoClipIndex] <= -2)
            {
                End();
            }
            else
            {
                IndicatorCanvas.SetActive(true);
                // Tekst Left Choice
                LeftChoiceTekst.GetComponent<TextMeshProUGUI>().text = TextLeftChoice[videoClipIndex];
                LeftChoiceTekst.transform.parent.parent.position = new Vector3(Original[0].x, -3, 10);
                LeftChoiceTekst.transform.parent.parent.gameObject.SetActive(true);
                // Tekst Center Choice
                CenterChoiceTekst.GetComponent<TextMeshProUGUI>().text = TextCenterChoice[videoClipIndex];
                CenterChoiceTekst.transform.parent.parent.position = new Vector3(Original[1].x, -3, 10);
                CenterChoiceTekst.transform.parent.parent.gameObject.SetActive(true);
                // Tekst Right Choice
                RightChoiceTekst.GetComponent<TextMeshProUGUI>().text = TextRightChoice[videoClipIndex];
                RightChoiceTekst.transform.parent.parent.position = new Vector3(Original[2].x, -3, 10);
                RightChoiceTekst.transform.parent.parent.gameObject.SetActive(true);
            }
        }
    }

    public void RayCast()
    {
        Ray ray = new Ray(camera.transform.position, transform.forward);
        Debug.DrawRay(camera.transform.position, transform.forward * 100);

        if(Physics.Raycast(ray, out RaycastHit hit, 100))
        {
            //Debug.Log("Hit!! " + hit.collider.tag);
            if(hit.collider.tag == "CenterChoice" && videoClipIndex != IndexCenterChoice[CurrentIndex])
            {
                Canvas.SetActive(true);
                CurrentValue = 0;

                videoClipIndex = IndexCenterChoice[CurrentIndex];
            }
            else if(hit.collider.tag == "RightChoice" && videoClipIndex != IndexRightChoice[CurrentIndex])
            {
                Canvas.SetActive(true);
                CurrentValue = 0;

                videoClipIndex = IndexRightChoice[CurrentIndex];
            }
            else if(hit.collider.tag == "LeftChoice" && videoClipIndex != IndexLeftChoice[CurrentIndex])
            {
                Canvas.SetActive(true);
                CurrentValue = 0;

                videoClipIndex = IndexLeftChoice[CurrentIndex];
            }
            else if(hit.collider.tag == "Reset")
            {
                Canvas.SetActive(false);
                CurrentValue = 0;

                videoClipIndex = 0;
            }
        }

        if(Canvas.activeSelf)
        {
            CurrentValue += Speed * Time.deltaTime;
            Image.fillAmount = CurrentValue / FillAmount;
        }

        if(CurrentValue >= FillAmount)
        {
            CurrentValue = 0;
            CurrentIndex = videoClipIndex;
            Canvas.SetActive(false);

            PlayVideo();
        }
    }

    public void PlayVideo()
    {
        if(videoClipIndex >= -1)
        {
            VideoIsDone = false;
            InvokeRepeating("CheckIfDone", .1f, .1f);

            // Plays the first video
            videoPlayer.clip = videoClips[videoClipIndex];
            videoPlayer.Play();
        }
        else
        {
            End();
        }
    }

    public void End()
    {
        IndicatorCanvas.SetActive(false);
        LeftChoiceTekst.transform.parent.parent.gameObject.SetActive(false);
        CenterChoiceTekst.transform.parent.parent.gameObject.SetActive(false);
        RightChoiceTekst.transform.parent.parent.gameObject.SetActive(false);

        Exit.transform.parent.gameObject.SetActive(true);

        if(videoClipIndex == -2)
        {
            Exit.GetComponent<TextMeshProUGUI>().text = "Slut";
        }
        else
        {
            Exit.GetComponent<TextMeshProUGUI>().text = "Slut";
        }
    }
}
